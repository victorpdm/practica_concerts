<?php

namespace AppBundle\Entity;

/**
 * Concert
 */
class Concert
{
    /**
     * @var string
     */
    private $titol;

    /**
     * @var string
     */
    private $grup;

    /**
     * @var string
     */
    private $data;

    /**
     * @var string
     */
    private $ciutat;

    /**
     * @var string
     */
    private $sala;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set titol
     *
     * @param string $titol
     *
     * @return Concert
     */
    public function setTitol($titol)
    {
        $this->titol = $titol;

        return $this;
    }

    /**
     * Get titol
     *
     * @return string
     */
    public function getTitol()
    {
        return $this->titol;
    }

    /**
     * Set grup
     *
     * @param string $grup
     *
     * @return Concert
     */
    public function setGrup($grup)
    {
        $this->grup = $grup;

        return $this;
    }

    /**
     * Get grup
     *
     * @return string
     */
    public function getGrup()
    {
        return $this->grup;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return Concert
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set ciutat
     *
     * @param string $ciutat
     *
     * @return Concert
     */
    public function setCiutat($ciutat)
    {
        $this->ciutat = $ciutat;

        return $this;
    }

    /**
     * Get ciutat
     *
     * @return string
     */
    public function getCiutat()
    {
        return $this->ciutat;
    }

    /**
     * Set sala
     *
     * @param string $sala
     *
     * @return Concert
     */
    public function setSala($sala)
    {
        $this->sala = $sala;

        return $this;
    }

    /**
     * Get sala
     *
     * @return string
     */
    public function getSala()
    {
        return $this->sala;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

